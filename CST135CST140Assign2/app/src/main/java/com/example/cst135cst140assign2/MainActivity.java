package com.example.cst135cst140assign2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //Attributes

    //Create variables for the elements on the activity_main.
    private Spinner spinSelectCourses;
    private TextView etClassCode, etClassName;
    private Button btnEditMarks, btnNew, btnDelete, btnSave;
    private RadioButton rdoYear1, rdoYear2;
    private RadioGroup rdoGroup;

    //Create variables the databases and cursor.
    private CourseDBHelper dbCourse;
    private AssessmentDBHelper dbAsses;
    private Cursor cursor;

    //Create variables for the SharedPreferences and its requires variables.
    private SharedPreferences preferences;
    public static final String radioSelect = "radioSelect";
    SharedPreferences.Editor editor;

    /**
     * This method is run when the application is opened.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Sets the view to the main activity */
        setContentView(R.layout.activity_main);

        /* Initialize shared preferences for storing default checked radio button*/
        preferences = getApplicationContext().getSharedPreferences("myPref", Context.MODE_PRIVATE); // 0 - for private mode

        /* Initialize the spinner */
        spinSelectCourses = findViewById(R.id.spinSelectCourses);
        spinSelectCourses.setOnItemSelectedListener(this);

        /* Initializes text fields/radio buttons */
        etClassCode = findViewById(R.id.etClassCode);
        etClassName = findViewById(R.id.etClassName);
        rdoGroup = findViewById(R.id.rdoGroup);

        /* Anonymous Listener for RadioGroup */
        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                /* initialized editor for storing boolean value */
                editor = preferences.edit();

                if(rdoYear1.isChecked()){
                    editor.putBoolean(radioSelect, true);
                } else {
                    editor.putBoolean(radioSelect, false);
                }
                editor.commit();
                Log.i("RADIO",preferences.getBoolean(radioSelect,true )+"");

            }
        });
        rdoYear1 = findViewById(R.id.rdoYear1);
        rdoYear2 = findViewById(R.id.rdoYear2);
        btnEditMarks = findViewById(R.id.btnEditMarks);
        btnNew = findViewById(R.id.btnNew);
        btnDelete = findViewById(R.id.btnDelete);
        btnSave = findViewById(R.id.btnSave);

        /* Initializes the databases */
        dbCourse = new CourseDBHelper(this);
        dbAsses = new AssessmentDBHelper(this);


        refreshData(); //Used update the spinner with all of the available courses in the Course Database.

        //When the activity is opened, this reads in the data from SharedPreferences to set which
        //Radio button the user used last.
        if(preferences.contains(radioSelect)){
            if(preferences.getBoolean(radioSelect,true)){
                rdoYear1.setChecked(true);
            } else {
                rdoYear2.setChecked(true);
            }
        }
    }

    /**
     * The Edit Marks, New, Delete Save button's onClick attributes in the layout are set to this method.
     * based on which button is clicked, (using the view object) will allow the user to clear the fields,
     * Delete a course/add a course from the Course database, or edit the marks of that course.
     * @param view Represents which button was clicked.
     */
    public void btnClick(View view) {

        switch (view.getId()) {

            case R.id.btnNew: //Prepare fields for new entry by emptying them.
                clearFields();
                break;

            case R.id.btnDelete: //Deletes a course in the Course Database.
                if(checkCourseExists(etClassCode.getText().toString())) { // Checks to see if the Course exists.
                    dbCourse.open();
                    Course course = new Course(
                            etClassCode.getText().toString(),
                            etClassName.getText().toString(),
                            rdoYear1.isChecked() ? "1" : "2"
                    );
                    dbCourse.deleteCourse(course);
                    refreshData();
                    clearFields();
                    dbCourse.close();
                }
                break;

            case R.id.btnSave: //Either adds a new Course in the Course Database or update an existing one
                if(checkCourseExists(etClassCode.getText().toString())) { // Checks to see if the Class Code exists to Update it in the Course Database.
                    dbCourse.open();
                    Course course = new Course(
                            etClassCode.getText().toString(),
                            etClassName.getText().toString(),
                            rdoYear1.isChecked() ? "1" : "2"
                    );
                    dbCourse.updateCourse(course);

                } else if(etClassCode.getText().length() > 0 ){ //Checks to see if the field isn't empty and Insert new Course in the Course Database.
                    dbCourse.open();
                    Course course = new Course(
                            etClassCode.getText().toString(),
                            etClassName.getText().toString(),
                            rdoYear1.isChecked() ? "1" : "2"
                    );

                    //Adds the course to the Course database and returns a id to be used as a Foreign key with the Assessment objects.
                    long courseID = dbCourse.createCourse(course);

                    //When a new course is created, 6 assessment objects are created using the courseID as their Foreign key to link
                    //the Assessment and Course objects together.
                    dbAsses.open();
                    dbAsses.createAssessment(new Assessment(courseID,"Final"));
                    dbAsses.createAssessment(new Assessment(courseID,"Midterm"));
                    for(int i = 1; i<5; i++){
                        dbAsses.createAssessment(new Assessment(courseID,"A" + i));
                    }
                    dbAsses.close();
                }

                refreshData();
                dbCourse.close();
                break;

            case R.id.btnEditMarks: //Takes users to a new activity where they can manage the courses Assessments.

                //Checks to see if the fields aren't empty.
                if(etClassCode.getText().length() > 0 || etClassName.getText().length() > 0){
                    dbCourse.open();

                    Intent intent = new Intent(this, Marks.class);

                    Course tempCourse = dbCourse.getCourse(etClassCode.getText().toString());

                    //Passes the courseID and courseCode of course the user is currently viewing.
                    intent.putExtra("courseID", tempCourse.id);
                    intent.putExtra("courseCode", tempCourse.code);

                    dbCourse.close();
                    startActivityForResult(intent, 1);
                }
                break;
        }
    }

    /**
     * When called, clearFields sets the two text fields in the activity to two empty strings,
     * removing any texts prior.
     */
    private void clearFields() {
        etClassCode.setText("");
        etClassName.setText("");
    }

    /**
     * Refresh data gets all off the course data and displays each course code/average in the spinner
     * from the database.
     */
    private void refreshData() {

        dbCourse.open();

        // Ensure the spinner has all of the rows from the table
        cursor = dbCourse.getAllCourses(); // Fill Cursor

        // The cursor adapter will be the link between the cursor and the spinner
        String[] cols = new String[] {dbCourse.CODE, dbCourse.AVG}; // this is a list of columns to show on the view (spinner)
        int [] views = new int [] {R.id.txtCode, R.id.txtAvg}; // list of views to place the data

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.spinner_view, cursor, cols, views);

        spinSelectCourses.setAdapter(adapter);
        dbCourse.close();
    }

    /**
     * When items are selected from the spinner, onItemSelected allows us grab the data from the cursor,
     * which will represent a single course, and we use that data to fill in the two edit feilds and
     * radio buttons. Unfortunately, we don't use any of the variables being passed into the object.
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // get the data from the cursor which has been moved by the adapter to point to the selected item
        etClassCode.setText(cursor.getString(1));
        etClassName.setText(cursor.getString(2));
        if(cursor.getString(3).equals("1")) {
            rdoYear1.setChecked(true);
        } else {
            rdoYear2.setChecked(true);
        }

    }

    /**
     * Does nothing, but is required to have because we are implementing AdapterView.OnItemSelectedListener
     * @param parent
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * checkCourseExists is a method that checks if the courseCode being passed in is an actual course
     * in the Course Database. It checks to see if the string actually exists, and checks the database
     * by calling .getCourse(). It returns a boolean because on if the existance of the course exists
     * or not.
     * @param courseCode using the course's courseCode to check if the course exits.
     * @return Returns a boolean based on if the courseCode being passed in exists in a course, in the
     * course database.
     */
    private boolean checkCourseExists(String courseCode) {

        if(courseCode.length() > 0) {
            dbCourse.open();
            Course tempCourse = dbCourse.getCourse(courseCode);

            if(tempCourse != null) {
                dbCourse.close();
                return true;
            } else {
                dbCourse.close();
                return  false;
            }

        }

        return false;


    }

    /**
     * When coming back from an activity, this methods listens for a specific requestcode and resultcode
     * to update the calculate average in the course's attributes, if the marks for any of the related
     * assessments changed.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == RESULT_CANCELED){
                try {
                    double returnVal = (double)(data.getFloatExtra("avg", 0));
                    Log.i("AVERAGE3",returnVal+"");

                    dbCourse.open();
                    Course currentCourse = dbCourse.getCourse(etClassCode.getText().toString());
                    currentCourse.courseAverage = returnVal;
                    dbCourse.updateCourse(currentCourse);
                    refreshData();
                    dbCourse.close();


                }catch (NullPointerException e){

                }
            }
        }
    }
}
