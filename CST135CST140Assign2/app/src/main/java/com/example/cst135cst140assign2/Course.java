package com.example.cst135cst140assign2;

/**
 * File Name:	Course.java
 * Author:		Michael Sabares CST135 | Joel Sipes CST140
 * Date:		05/27/2019
 * Purpose:	    The Course object represents the courses the user can add and remove from the database.
 */

public class Course {

    public long id; //Primary Key
    public String code;
    public String name;
    public String year;
    public double courseAverage;

    /**
     * This constructor is used when a user Saves a new course into the database. Attributes like id
     * and courseAverage are already initalized.
     * @param code The course code of the object.
     * @param name The name of the object.
     * @param year The year of the object.
     */
    public Course (String code, String name, String year){

        this.id = -1;
        this.code = code;
        this.name = name;
        this.year = year;
        this.courseAverage = 0;
    }

    /**
     * This constructor is used when retrieving a course from the database.
     * @param id Primary Key.
     * @param code The course code of the object.
     * @param name The name of the object.
     * @param year The year of the object.
     * @param avg The calculated average mark of the class.
     */
    public Course (long id, String code, String name, String year, double avg){

        this(code,name,year);

        this.id = id;
        this.courseAverage = avg;
    }

}
