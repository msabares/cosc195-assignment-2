package com.example.cst135cst140assign2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CourseDBHelper extends SQLiteOpenHelper {

    // The Attributes that will make up the Course Database.
    public static final String DB_NAME = "courses.db",
            TABLE_NAME = "Courses",
            ID = "_id",
            CODE = "code",
            NAME = "name",
            YEAR = "year",
            AVG = "average";

    public static final int DB_VERSION = 1;

    public SQLiteDatabase sqlDB; // Reference to the SQLite database on the file system

    //Constructor
    /**
     * The name of the database and the version number is already initialized for the purpose to
     * create a Course Database.
     * @param context Requires the context of the activity you are in.
     */
    public CourseDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Methods

    /**
     * Automatically is called if the
     * database file does not already exists and creates the Course Table
     * @param db Used to call .execSQL to create the Course Table.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE " +
                TABLE_NAME + "(" +
                ID + " integer primary key autoincrement, " +
                CODE + " text not null," +
                NAME + " text not null," +
                YEAR + " text not null," +
                AVG + " double not null);";
        db.execSQL(create);
    }

    /**
     * Automatically called with the version # increases. onUpgrade will drop the existing table
     * and call onCreate() to create a new table, removing all the data.
     * @param db Used to call .execSQL drop the Course Table.
     * @param oldVersion old Version Number
     * @param newVersion new Version Number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drops existing table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Recreates it
        onCreate(db);
    }

    /**
     * When working with the database, you'll need to call this method so the database is writable?
     * @throws SQLException
     */
    public void open() throws SQLException {
        sqlDB = this.getWritableDatabase();
    }

    /**
     * When you are no longer working with the database, calling this method will prevent the
     * database to be writable?
     */
    public void close() {
        sqlDB.close();
    }

    /**
     * Takes in an Course object so a ContentValues object can put the attributes of that object
     * into the database using .insert() method from SQLiteDatabase. This method also sets and returns
     * the auto increment value into the database.
     * @param course The Course object you wish to add into the Course Database.
     * @return Returns the auto increment id of the object that was placed into the Course Database.
     */
    public long createCourse(Course course) {

        // A Container to store each column and value
        ContentValues cvs = new ContentValues();

        // Add an item for each column and value
        cvs.put(CODE, course.code);
        cvs.put(NAME, course.name);
        cvs.put(YEAR, course.year);
        cvs.put(AVG, course.courseAverage);

        // Execute insert, which returns the auto increment value
        long autoid = sqlDB.insert(TABLE_NAME, null, cvs);

        // Update the id of the course with the new auto id
        course.id = autoid;
        return autoid;
    }

    /**
     * Takes in an Course object and uses a ContentValues object to update an existing Course
     * object in the database using .update() method from SQLiteDatabase.
     * @param course The Assessment object you wish to update in the Course Database.
     * @return Returns a confirmation if the update was successful.
     */
    public boolean updateCourse(Course course) {

        // A Container to store each column and value
        ContentValues cvs = new ContentValues();

        // Add an item for each column and value
        cvs.put(CODE, course.code);
        cvs.put(NAME, course.name);
        cvs.put(YEAR, course.year);
        cvs.put(AVG, course.courseAverage);

        return sqlDB.update(TABLE_NAME, cvs, CODE + " = '" + course.code + "'", null) > 0;
    }

    /**
     * Takes in a Course object and uses SQLiteDatabase method .delete() to find that object in the database
     * and delete it.
     * @param course The Course object you wish to delete from the Course Database.
     * @return Returns a confirmation if the delete was successful.
     */
    public boolean deleteCourse(Course course) {
        return sqlDB.delete(TABLE_NAME, CODE + " = '" + course.code + "'", null) > 0;
    }

    /**
     * A method that will return the whole Course Database as a Cursor object to be used to display
     * all of the available courses in the Spinner
     * @return Returns a Course object that be used to find the all of the courses in the Course Database.
     */
    public Cursor getAllCourses() {

        // List of columns to select and return
        String[] sFields = new String [] {ID, CODE, NAME, YEAR, AVG};
        return sqlDB.query(TABLE_NAME, sFields, null, null, null, null, null);
    }

    /**
     * A method that finds a course in the Course Database using the courseCode and return that course
     * as a Course object.
     * @param courseCode The unique identifier you wish to find the desired course.
     * @return Returns a Course object or null if no it wasn't able to find the course in the Course Database
     */
    public Course getCourse(String courseCode) {
        String[] sFields = new String [] {ID, CODE, NAME, YEAR, AVG};
        Cursor fpCursor = sqlDB.query(TABLE_NAME, sFields, CODE + " = '" + courseCode + "'", null, null, null, null, null);

        if(fpCursor != null && fpCursor.getCount() > 0) { // check for a found result
            // move to the first record
            fpCursor.moveToFirst();
            return new Course(fpCursor.getLong(0), fpCursor.getString(1), fpCursor.getString(2), fpCursor.getString(3), fpCursor.getDouble(4));
        }
        return null;
    }

}
