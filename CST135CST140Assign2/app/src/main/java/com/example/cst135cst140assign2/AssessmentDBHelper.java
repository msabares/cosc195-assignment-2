package com.example.cst135cst140assign2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class AssessmentDBHelper extends SQLiteOpenHelper {

    // The Attributes that will make up the Assessment Database.
    public static final String DB_NAME = "assessment.db",
            TABLE_NAME = "Assessments",
            ID = "_id",
            COURSEID = "courseId",
            NAME = "name",
            WEIGHT = "weight",
            MARK = "mark";
    public static final int DB_VERSION = 1;

    public SQLiteDatabase sqlDB; // Reference to the SQLite database on the file system

    //Constructor
    /**
     * The name of the database and the version number is already initialized for the purpose to
     * create an Assessment Database.
     * @param context Requires the context of the activity you are in.
     */
    public AssessmentDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Methods

    /**
     * Automatically is called if the
     * database file does not already exists and creates the Assessment Table
     * @param db Used to call .execSQL to create the Assessment Table.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE " +
                TABLE_NAME + "(" +
                ID + " integer primary key autoincrement, " +
                COURSEID + " long not null," +
                NAME + " text not null," +
                WEIGHT + " text not null," +
                MARK + " text not null);";
        db.execSQL(create);
    }

    /**
     * Automatically called with the version # increases. onUpgrade will drop the existing table
     * and call onCreate() to create a new table, removing all the data.
     * @param db Used to call .execSQL drop the Assessment Table.
     * @param oldVersion old Version Number
     * @param newVersion new Version Number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drops existing table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Recreates it
        onCreate(db);
    }

    /**
     * When working with the database, you'll need to call this method so the database is writable?
     * @throws SQLException
     */
    public void open() throws SQLException {
        sqlDB = this.getWritableDatabase();
    }

    /**
     * When you are no longer working with the database, calling this method will prevent the
     * database to be writable?
     */
    public void close() {
        sqlDB.close();
    }

    /**
     * Takes in an Assessment object so a ContentValues object can put the attributes of that object
     * into the database using .insert() method from SQLiteDatabase. This method also sets and returns
     * the auto increment value into the database.
     * @param assessment The Assessment object you wish to add into the Assessment Database.
     * @return Returns the auto increment id of the object that was placed into the Assessment Database.
     */
    public long createAssessment(Assessment assessment) {

        // A Container to store each column and value
        ContentValues cvs = new ContentValues();

        // Add an item for each column and value
        cvs.put(COURSEID, assessment.courseId);
        cvs.put(NAME, assessment.name);
        cvs.put(WEIGHT, assessment.weight);
        cvs.put(MARK, assessment.mark);

        // Execute insert, which returns the auto increment value
        long autoid = sqlDB.insert(TABLE_NAME, null, cvs);

        // Update the id of the assessment with the new auto id
        assessment.id = autoid;
        return autoid;
    }

    /**
     * Takes in an Assessment object and uses a ContentValues object to update an existing Assessment
     * object in the database using .update() method from SQLiteDatabase.
     * @param assessment The Assessment object you wish to update in the Assessment Database.
     * @return Returns a confirmation if the update was successful.
     */
    public boolean updateAssessment(Assessment assessment) {

        // A Container to store each column and value
        ContentValues cvs = new ContentValues();

        // Add an item for each column and value
        cvs.put(COURSEID, assessment.courseId);
        cvs.put(NAME, assessment.name);
        cvs.put(WEIGHT, assessment.weight);
        cvs.put(MARK, assessment.mark);

        //Where clause is based on the object ID
        return sqlDB.update(TABLE_NAME, cvs, ID + " = " + assessment.id , null) > 0;
    }


    /**
     * A method that takes in a course ID and returns all of the Assessment objects that contains the
     * course ID.
     * @param courseid The course ID of the course. Used to determine which Assessment objects to retrieve.
     * @return Returns an arrayList of Assessment objects.
     */
    public ArrayList<Assessment> getAllAssessmentsFromCourse(long courseid) {

        ArrayList<Assessment> tempArray = new ArrayList<>();

        // List of columns to select and return
        String[] sFields = new String [] {ID, COURSEID, NAME, WEIGHT, MARK};
        Cursor cursor = sqlDB.query(TABLE_NAME, sFields, COURSEID + " = " + courseid, null, null, null, null);

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            tempArray.add(new Assessment(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getInt(3), cursor.getInt(4)));
        }

        return tempArray;
    }


}
