package com.example.cst135cst140assign2;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Marks extends AppCompatActivity implements AdapterView.OnItemClickListener {

    //Attributes

    //Create variables for the elements on the activity_marks.
    private TextView txtCourseCode;
    private EditText etWeight, etMark;
    private ListView lvMarks;
    private Button btnSave;

    private long courseID;
    private AssessmentDBHelper dbAsses;

    private ArrayList<Assessment> markList;
    private Assessment assessment;
    private ArrayAdapter adapter;

    float average = 0;

    Intent intent;

    /**
     * This method is run when the application is opened.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marks);

        //Initialize attributes
        txtCourseCode = findViewById(R.id.txtCourseCode);
        etWeight = findViewById(R.id.etWeight);
        etMark = findViewById(R.id.etMark);
        lvMarks = findViewById(R.id.lvMarks);
        btnSave = findViewById(R.id.btnSave);

        //Initialize the database
        dbAsses = new AssessmentDBHelper(this);


        //When this activity is called, it is opened using an intent, which is also being passed
        //two key value pairs
        Bundle bundle = getIntent().getExtras();
        courseID = bundle.getLong("courseID");
        txtCourseCode.setText(bundle.getString("courseCode"));

        //getAllAssessmentsFromCourse() gets us all of the Assessment objects related to the course
        //ID we are working with.
        dbAsses.open();
        markList = dbAsses.getAllAssessmentsFromCourse(courseID);
        dbAsses.close();


        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, markList);
        lvMarks.setAdapter(adapter);

        //set a setOnItemClickListener to the lists so we can interact with the list.
        lvMarks.setOnItemClickListener(this);

        intent = new Intent();


    }


    /**
     * When one of the items in the lists are clicked on, it will populate the two edit text fields.
     * Unfortunately, we don't use any of the variables being passed into the object.
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        assessment = (Assessment)lvMarks.getItemAtPosition(position);
        etWeight.setText(String.valueOf(assessment.weight));
        etMark.setText(String.valueOf(assessment.mark));
    }

    /**
     * The Save button's onClick attributes in the layout are set to this method. Whenever save is clicked
     * and the fields valid, it will update the appropriate Assessment object in the Assessment Database
     * and update the current listview to reflect the changes made.
     * @param view
     */
    public void btnClick(View view) {

        if(!etWeight.getText().toString().equals("") || !etMark.getText().toString().equals("")){
            dbAsses.open();
            assessment.weight = Integer.parseInt(etWeight.getText().toString());
            assessment.mark = Integer.parseInt(etMark.getText().toString());

            dbAsses.updateAssessment(assessment);
            lvMarks.setAdapter(adapter);

           dbAsses.close();
        }
        else
        {
            Toast.makeText(this, "Fields Must Not Be Blank", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * When the user goes back to the previous activity, we will calculate the course's average and pass
     * that data on as an intent.
     */
    @Override
    public void onBackPressed() {

        dbAsses.open();

        float weight = 0;

        markList = dbAsses.getAllAssessmentsFromCourse(courseID);

        for(Assessment asses : markList){
            weight = (float)(asses.weight)/(float)(100);
            Log.i("WEIGHT", weight+"");
            Log.i("WEIGHT", asses.mark+"");
            average = average + (weight * (float)asses.mark);
            Log.i("AVERAGE", average+"");
        }
        Log.i("AVERAGE2", average+"");

        intent.putExtra("avg",average);

        dbAsses.close();
        setResult(RESULT_CANCELED, intent);
        super.onBackPressed();
    }

}
