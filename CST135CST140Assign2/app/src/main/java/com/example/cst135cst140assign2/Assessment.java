package com.example.cst135cst140assign2;

/**
 * File Name:	Assessment.java
 * Author:		Michael Sabares CST135 | Joel Sipes CST140
 * Date:		05/27/2019
 * Purpose:	    The Assessment objects represents the Final, Midterm A1-A4 of marks for each Course object.
 */

public class Assessment {

    public long id; //Primary Key
    public long courseId; // Foreign Key
    public String name;
    public int weight;
    public int mark;

    /**
     * This constructor is used when retrieving data from the database and turning them into Assessment objects.
     * @param id Uniqu Primary Key.
     * @param courseId Foreign key that associates a Assessment object with a Course object.
     * @param name Name of the assessment.
     * @param weight Weight of the assessment (Takes in an int but will be converted into a float to represent a percentage)
     * @param mark Mark of the assessment.
     */
    public Assessment(long id, long courseId, String name, int weight, int mark) {
        this.id = id;
        this.courseId = courseId;
        this.name = name;
        this.weight = weight;
        this.mark = mark;
    }

    /**
     * This constructor is used when we are creating an assessment object to be placed into the database.
     * A lot of the initial values are initialized to a default value that will be changed later.
     * @param courseId Foreign key that associates a Assessment object with a Course object.
     * @param name Name of the assessment.
     */
    public Assessment(long courseId, String name) {
        this.id = -1;
        this.courseId = courseId;
        this.name = name;
        this.weight = 0;
        this.mark = 0;
    }

    /**
     * This method is used to display all of the relevant values to be displayed in the List View
     * @return returns a string that contains the name, weight, and mark of the Assessment object.
     */
    @Override
    public String toString() {
        return this.name + " \tW: " + this.weight + " \tMark: "+this.mark;
    }

}
